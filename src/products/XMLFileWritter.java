package products;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLFileWritter extends Product {

	@Override
	public void writeNewFile(String fileName) {
		
		List<List<String>> toWrite = new ArrayList<>();
		super.lines.stream().forEachOrdered(o -> toWrite.add(new ArrayList<String>(Arrays.asList(o.split("\\|")))));
		
		try {
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("Elements");
			doc.appendChild(rootElement);
			
			toWrite.stream().forEachOrdered(o -> {
				Element line = doc.createElement("subelements");
				for (String temp : o)
					line.appendChild(doc.createTextNode(temp +"\n"));
				rootElement.appendChild(line);
			});
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName + ".xml"));
			
			transformer.transform(source, result);
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		}
	}

}
