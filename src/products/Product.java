package products;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public abstract class Product {

	protected List<String> lines;
	
	public Product() {
		lines = new ArrayList<String>();
	}
	
	public Boolean validInput(String path) throws IOException {
		try (Stream<String> stream = Files.lines(Paths.get(path))) {

			stream.forEach(o -> lines.add(o));

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public abstract void writeNewFile(String fileName);
	
}
