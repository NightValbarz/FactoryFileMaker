package products;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TabulationTextFileWritter extends Product {

	@Override
	public void writeNewFile(String fileName) {
		try {
			PrintWriter pw = new PrintWriter(fileName.concat(".txt"), "UTF-8");
			super.lines.stream().forEachOrdered(o -> pw.write(o.replaceAll("\\|", "\t") + "\n"));
			pw.close();
			
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
