package products;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonFileWritter extends Product {

	@Override
	public void writeNewFile(String fileName) {
		ObjectMapper mapper = new ObjectMapper();
		File file = new File(fileName + ".json");
		
		List<List<String>> toWrite = new ArrayList<>();
		
		super.lines.stream().forEachOrdered(o -> toWrite.add(new ArrayList<String>(Arrays.asList(o.split("\\|")))));
		
		try {
			
	        mapper.writeValue(file, toWrite);

	    } catch (IOException e) {  
	        e.printStackTrace();  
	    }  
		
	}

}
