package factory;

import enumtypes.FileType;
import products.Product;

public abstract class FactoryFile {

	public abstract Product createConverter(FileType type);
}
