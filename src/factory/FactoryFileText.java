package factory;

import enumtypes.FileType;
import products.CSVFileWritter;
import products.JsonFileWritter;
import products.Product;
import products.TabulationTextFileWritter;
import products.XMLFileWritter;

public class FactoryFileText extends FactoryFile {

	@Override
	public Product createConverter(FileType type) {

		switch (type) {

		case TABULATION_TEXT:
			return new TabulationTextFileWritter();

		case CSV:
			return new CSVFileWritter();

		case XML:
			return new XMLFileWritter();

		case JSON:
			return new JsonFileWritter();

		default:
			break;
		}
		return null;
	}

}
