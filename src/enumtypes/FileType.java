package enumtypes;

public enum FileType {

	TABULATION_TEXT,
	CSV,
	XML,
	JSON
	
}
