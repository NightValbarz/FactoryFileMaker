package application;

import java.io.IOException;

import enumtypes.FileType;
import factory.FactoryFile;
import factory.FactoryFileText;
import products.Product;

public class App {

	public static void main(String[] args) {
		
		FactoryFile factory = new FactoryFileText();
		Product sample1 = factory.createConverter(FileType.TABULATION_TEXT);
		Product sample2 = factory.createConverter(FileType.CSV);
		Product sample3 = factory.createConverter(FileType.JSON);
		Product sample4 = factory.createConverter(FileType.XML);
		
		try {
			if (sample1.validInput("inputTest.txt"))
				sample1.writeNewFile("TabulationSample");
			if (sample2.validInput("inputTest.txt"))
				sample2.writeNewFile("CSVSample");
			if (sample3.validInput("inputTest.txt"))
				sample3.writeNewFile("JsonSample");
			if (sample4.validInput("inputTest.txt"))
				sample4.writeNewFile("XMLSample");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
